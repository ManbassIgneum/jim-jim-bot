# coding:cp1251/utf8
# Python 3.6
import requests
import urllib.request
import re
import heapq
import os
from collections import Counter
from collections import namedtuple

#src - папка для хранения временных файлов (которые, после обработки, удаляются), туда же добавляются результирующие файлы
src = 'E:/Python36/proj/jimjimscript/'

class List(namedtuple('List', ['ch'])):
    def roam(self, a, b):
        a[self.ch] = b or "0"


class Node(namedtuple('Node', ['l', 'r'])):
    def roam(self, a, b):
        self.l.roam(a, b + '0')
        self.r.roam(a, b + '1')


def encode(s):
    h = []
    for ch, freq in Counter(s).items():
            h.append((freq, len(h), List(ch)))
    heapq.heapify(h)
    count = len(h)
    while len(h) > 1:
        freq1, _count1, l = heapq.heappop(h)
        freq2, _count2, r = heapq.heappop(h)
        heapq.heappush(h, (freq1 + freq2, count, Node(l, r)))
        count += 1
    dict = {}
    if h:
        [(_freq, _count, root)] = h
        root.roam(dict, "")
    return dict


def decode(encoded, dict):
    str = ''
    cur = ''
    encoded.encode()
    for ch in encoded:
        cur += ch
        for dec_ch in dict:
            if dict.get(dec_ch) == cur:
                str += dec_ch
                cur = ''
                break
    return str


def decoding(nam):
    g = open(nam, 'rb')
    dict = {}
    mum, num, code = g.readline().split(b'|')
    num = int(num)
    mum = int(mum)
    code = int(code)
    cod = 0
    sizes = g.readline().split(b'|')
    names = g.readline().split(b'|')
    for el in range(mum):
        sizes[el] = int(sizes[el])
        cod += sizes[el]
    curr = 0
    s = ''
    st = g.read().split(b'#', num)
    for i in range(len(st) - 1):
        a, b = st[i].split(b'|')
        a = a.decode('utf-8')
        b = b.decode('utf-8')
        dict.update([(a, b)])
    encoded = ''
    for i in st[len(st) - 1]:
        encoded += bin(i)[2:].zfill(8)
        curr += 8
    while curr > code:
        encoded = encoded[:len(encoded) - 1]
        curr -= 1
    decoded = decode(encoded, dict)
    now = 0
    siz = 0
    for i in range(len(names) - 1):
        try:
            f = open(src + str(names[i].decode(encoding='utf8')), 'w', encoding='cp1251')
        except:
            f = open(src + str(names[i].decode(encoding='utf8')), 'w', encoding='utf-8')
        siz += sizes[i]
        f.write(decoded[now:siz])
        now += sizes[i]
        f.close()
    g.close()


def encoding_few(nam):
    sit = urllib.request.urlopen(nam).read()
    o = re.compile('href="([^"]*txt[^"]*)"')
    sit = str(sit, 'utf-8', errors='ignore')
    starturl = nam
    chs = 0
    while starturl[chs] != '/':
        chs += 1
    chs += 2
    while starturl[chs] != '/':
        chs += 1
    starturl = starturl[:chs]
    l = o.findall(sit)
    adress = []
    for i in range(len(l)):
        if l[i][len(l[i]) - 1] == '/':
            continue
        if l[i][:4] == 'http':
            adress.append(l[i])
        else:
            adress.append(starturl + l[i])
    mass = []
    sizz = []
    greatstr = b''
    for i in adress:
        try:
            r = requests.head(i)
            if r.headers['content-type'] == 'application/force-download':
                mass.append([urllib.request.urlopen(i).read(),
                             r.headers['content-disposition'][21:len(r.headers['content-disposition'])]])
            elif r.headers['content-type'][:10] == 'text/plain':
                mass.append([urllib.request.urlopen(i).read(),
                             r.headers['content-disposition'][
                             22:len(r.headers['content-disposition']) - 1]])
        except:
            continue
    for i in mass:
        with open(src + i[1], 'wb') as new_file:
            new_file.write(i[0])
        try:
            with open(src + i[1], 'r', encoding='utf-8') as new_file:
                temp = len(new_file.read())
            sizz.append([str(temp), i[1]])
        except:
            with open(src + i[1], 'r', encoding='cp1251') as new_file:
                temp = len(new_file.read())
            sizz.append([str(temp), i[1]])
        greatstr += i[0]
        os.remove(src + i[1])
    name = 'all_in_one.txt'
    with open(src + name, 'wb') as new_file:
        new_file.write(greatstr)
    normalmsg = ''
    for ch in nam[7:]:
        if ch != '/' and ch != '?' and ch != ':' and ch != '\\' and ch != '"' and ch != '*' and ch != '<' and ch != '>':
            normalmsg += ch
        else:
            normalmsg += '_'
    try:
        f = open(src + name, 'r', encoding='utf8')
        s = f.read()
    except:
        f = open(src + name, 'r', encoding='cp1251')
        s = f.read()
    g = open(src + normalmsg + '.jim', 'wb')
    dict = encode(s)
    encoded = ''
    counted = 0
    cu = 0
    prev = 0
    for i in range(len(s)):
        encoded += str(dict.get(s[i]))
    enencoded = bytearray()
    da = ''
    da += str(len(sizz)) + '|' + str(len(dict)) + '|' + str(len(encoded)) + '\n'
    for i in(sizz):
        da += str(i[0]) + '|'
    da += '\n'
    for i in(sizz):
        da += str(i[1]) + '|'
    da += '\n'
    for ch in sorted(dict):
        da += str(ch + '|' + dict[ch] + '#')
    enencoded.extend(bytes(da, encoding="utf-8"))
    i = 0
    while i < len(encoded):
        j = 0
        cur = 0
        while i < len(encoded) and j < 8:
            cur += int(encoded[i]) * (2 ** (7 - j))
            i += 1
            j += 1
        enencoded.append(cur)
    g.write(enencoded)
    f.close()
    g.close()
    os.remove(src + name)


def encoding(srcc):
    i = 0
    name = ''
    while i < len(srcc):
        name += srcc[i]
        if srcc[i] == '\\':
            name = ''
        i += 1
    try:
        f = open(srcc, 'r', encoding='utf8')
        s = f.read()
    except:
        f = open(srcc, 'r', encoding='cp1251')
        s = f.read()
    g = open(src + name[:len(name) - 3] + 'jim', 'wb')
    dict = encode(s)
    encoded = ''
    for i in range(len(s)):
        encoded += str(dict.get(s[i]))
    i = 0
    enencoded = bytearray()
    da = ''
    da += '1|' + str(len(dict)) + '|' + str(len(encoded)) + '\n'
    da += str(len(s)) + '|'
    da += '\n'
    da += str(name) + '|'
    da += '\n'
    for ch in sorted(dict):
        da += str(ch + '|' + dict[ch] + '#')
    enencoded.extend(bytes(da, encoding="utf-8"))
    while i < len(encoded):
        j = 0
        cur = 0
        while i < len(encoded) and j < 8:
            cur += int(encoded[i]) * (2 ** (7 - j))
            i += 1
            j += 1
        enencoded.append(cur)
    g.write(enencoded)
    f.close()
    g.close()


if __name__ == '__main__':
    costil = True
    while costil:
        print("For encoding enter 'e', for decoding - 'd', for exit - 'x':\n")
        char = input()
        if char == 'e':
            print("For encoding from file, enter 'f', from site - 's':")
            char = input()
            if char == 'f':
                print("Enter file path:")
                nam = input()
                try:
                    encoding(nam)
                except:
                    print('Wrong file or file path')
            elif char == 's':
                print("Enter site adress:")
                nam = input()
                try:
                    encoding_few(nam)
                except:
                    print('Wrong site')
            else:
                print("Wrong input")
        elif char == 'd':
            print("Enter file path:")
            nam = input()
            try:
                decoding(nam)
            except:
                print('Wrong file or file path')
        elif char == 'x':
            costil = False
        else:
            print("Wrong input")